/**
 * Configuration Service
 */
define([
    'app'
], function(app) {
    app.value('configuration', {
        /**
         * API configuration
         */
        api : {
            /**
             * Base domain for all ajax requests
             */
            domain : 'http:///localhost:8080/rest',

            /**
             * Base domain for all images
             */
            host: 'http://localhost:8080/',

            /**
             * Force to prefix the domain for all ajax calls (not yet
             * implemented)
             */
            forceDomain : true,

            /**
             * Don't cache GET requests!
             */
            cache : false,

            /**
             * Timeout in ms
             */
            timeout : 10000,

            /**
             * Endpoints for /catalogue
             */
            catalogue : {
                fetchAll : '/catalogue/DE/DE/',
                fetchSprite : '/catalogue/DE/DE/sprite',
                fetchSpriteDebug : '/catalogue/DE/DE/sprite/debug'

            },

            /**
             * Endpoints for /posDetails
             */
            pos : {
                fetchOne : '/pos/get/'
            },

            /**
             * Endpoints for /order
             */
            order : {
                fetchOne : '/order/get/',
                initializeOrder : '/order/initialize/',
                debugChangeState : '/order/debug/set/',
                abortOrder : '/order/abort/'
            },

            /**
             * Endpoints for /vault
             */
            vault : {
                get : '/vault/get/',
                getLatest : '/vault/latest/',
                redeem : '/vault/redeem/',
                unredeem : '/vault/unredeem/'
            },

            customer : {
                getInstallationId : '/customer/get/installationid'
            }

        },

        /**
         * Controller specific configuration
         */
        controllers : {

            /**
             * Order defaults
             */
            order : {
                /**
                 * In ms, how often we should poll the backend
                 */
                pollingInterval : 2000
            }
        }

    });

    return app;
});
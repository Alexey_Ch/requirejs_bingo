define(['app'], function (app) {
    return app.service('gameService', ['$http', 'configuration', function ($http, configuration) {

        console.log("gameService.construct()");
        var GAME_CONFIG = (function () {
            var private = {
                'MIN_VALUE': 1,
                'MAX_VALUE': 60,
                'CELL_AMOUNT_X': 5,
                'CELL_AMOUNT_Y': 5
            };

            return {
                get: function (name) {
                    return private[name];
                }
            };
        })();

        var clickSnd = new Audio("audio/click.mp3");

        var winSnd = new Audio("audio/win.mp3");

        var games = [];

        var usedNumbers = [];

        var field = [];

        function isWin(field, usedNumbers) {
            var result = false;
            var middleX = getMiddleX();
            var middleY = getMiddleY();
            var row;
            // 1 - free cell in center of card
            if (usedNumbers.length >= (GAME_CONFIG.get('CELL_AMOUNT_X') * GAME_CONFIG.get('CELL_AMOUNT_Y')) - 1) {
                console.log("Lenght > 24 ");
                for (var y = 0; y < field.length; y++) {
                    row = field[y];
                    for (var x = 0; x < row.length; x++) {
                        console.log("check " + row[x].value);
                        if (usedNumbers.indexOf(row[x].value) == -1) {
                            if(row[x].value != 'FREE') {
                                console.log("FALSE: arr item: " + (y * 5 + x) + ", value: " + row[x].value);
                                return false;
                            }
                        }
                    }
                }
                result = true;
            }
            console.log("RESULT: " + result);
            return result;
        }

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function getRandomOne() {
            return 1;
        }

        function getMiddleX() {
            return Math.floor(GAME_CONFIG.get('CELL_AMOUNT_X') / 2);
        }

        function getMiddleY() {
            return Math.floor(GAME_CONFIG.get('CELL_AMOUNT_Y') / 2);
        }

        function generateField() {
            var field = new Array();
            var row;
            var cell;
            console.log("CELL_AMOUNT_Y: " + GAME_CONFIG.get('CELL_AMOUNT_Y'));
            console.log("CELL_AMOUNT_X: " + GAME_CONFIG.get('CELL_AMOUNT_X'));
            var used = new Array();
            for (var y = 0; y < GAME_CONFIG.get('CELL_AMOUNT_Y'); y++) {
                console.log("ROW: " + y);
                row = new Array();
                for (var i = 0; i < GAME_CONFIG.get('CELL_AMOUNT_X'); i++) {
                    console.log("COLUMN: " + i);
                    cell = new Object();
                    cell.selected = '';
                    if ((i == getMiddleX()) && (y == getMiddleY())) {
                        console.log(" MIDDLE ");
                        cell.value = "FREE";
                        cell.selected = 'selected';
                    } else {
                        cell.value = getNextNumber(used);/*getRandomOne();getRandomInt(GAME_CONFIG.get('MIN_VALUE'), GAME_CONFIG.get('MAX_VALUE')); */
                    }
                    cell.x = i;
                    cell.y = y;
                    console.log("CELL[" + cell.value + "]");
                    row.push(cell);
                }
                field.push(row);
                console.log("--------------------------------");
                console.log(field);
            }
            return field;
        }

        function isContain(field, number) {
            var result = new Object();
            result.status = false;
            var row;
            for (var i = 0; i < field.length; i++) {
                row = field[i];
                for (var j = 0; j < row.length; j++) {
                    if (row[j].value == number) {
                        result.status = true;
                        result.x = j;
                        result.y = i;
                        return result;
                    }
                }
            }
            return result;
        }

        function gameLoop(game, scope) {

            console.log('GAME LOOP');
            console.log('GAME ID: ' + game.id);
            var number = getNextNumber(game.usedNumbers);
            scope.$apply();
            console.log('GAME [' + game.id + '] next: ' + number);
            var result = isContain(game.field, number);
            console.log('GAME [' + game.id + '] isContain: ' + result.status);

            if (result.status == true) {
                console.log('GAME [' + game.id + '] x, y: ' + result.x + ', ' + result.y);
                console.log('GAME [' + game.id + '] animate: ');
                console.log('CELL: ' + game.field[result.y][result.x].selected);
                clickSnd.load();
                clickSnd.play();
                game.field[result.y][result.x].selected = 'selected';
                console.log('CELL: ' + game.field[result.y][result.x].selected);
                scope.$apply();

            }
            console.log('GAME [' + game.id + '] field');
            console.log(game.field);
            console.log('GAME [' + game.id + '] usedNumbers');
            console.log(game.usedNumbers);

            if (isWin(game.field, game.usedNumbers)) {
                console.log('GAME [' + game.id + '] WIN: ');
                game.status = 'win';
                game.winText = 'Winner';
                winSnd.play();
                clearInterval(game.timer);
                scope.$apply();
            } else {
                console.log('GAME [' + game.id + '] PLAY YET: ');
            }
        }

        this.getFieldModel = function () {
            field = generateField();
            return field;

        }

        function getNextNumber(usedNumbers) {
            var max = GAME_CONFIG.get('MAX_VALUE');
            var nextNumber = getRandomInt(GAME_CONFIG.get('MIN_VALUE'), GAME_CONFIG.get('MAX_VALUE'));
            while (usedNumbers.length < max && usedNumbers.indexOf(nextNumber) != -1) {
                nextNumber = getRandomInt(GAME_CONFIG.get('MIN_VALUE'), GAME_CONFIG.get('MAX_VALUE'));
            }
            if (usedNumbers.length < max) {
                usedNumbers.push(nextNumber);
            }

            return nextNumber;
        }

        this.getUsedNumbers = function () {
            return usedNumbers;
        }

        this.checkWin = function (gameId) {
            return isWin(field, usedNumbers);
        }

        this.createGame = function (roomId, scope) {
            console.log("createGame with id: " + roomId);
            var game = new Object();
            game.status = '';
            game.winText = 'Bingo';
            game.field = generateField();
            game.usedNumbers = new Array();
            game.id = roomId;
            games.push(game);
            game.timer = setInterval(function () {
                gameLoop(game, scope);
            }, 2000);
            return game;
        }

    }]);
});
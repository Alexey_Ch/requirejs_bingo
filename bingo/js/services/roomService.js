define(['app'], function(app) {
    return app.service('roomService', ['$http', 'configuration', function($http, configuration) {

        console.log("roomService.construct()");
        var rooms = [
                {id: 1, title: 'room 1'},
                {id: 2, title: 'room 2'},
                {id: 3, title: 'room 3'},
                {id: 5, title: 'room 4'},
                {id: 6, title: 'room 5'},
                {id: 7, title: 'room 6'},
                {id: 8, title: 'room 7'}
            ];

            this.getRoomList = function () {
                return rooms;
            }

    }]);
});
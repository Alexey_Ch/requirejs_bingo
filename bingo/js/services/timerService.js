define(['app'], function(app) {
    return app.service('timerService', ['$http', 'configuration', function($http, Configuration) {

        console.log("timerService.construct()");
        var timers = [];

        this.addTimer = function (funcName, time) {
            var timer = setInterval(funcName, time);
            timers.push(timer);
        }

        this.deleteTimers = function () {
            for (var i = 0; i < timers.length; i++) {
                clearInterval(timers[i]);
            }
        }

    }]);
});
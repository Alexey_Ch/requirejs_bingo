define(['app'], function(app) {
    return app.service('playerService', ['$http', 'configuration', function($http, configuration) {

        console.log("playerService.construct()");

        var players = [
            {id: 1, title: 'player 1'},
            {id: 2, title: 'player 2'},
            {id: 3, title: 'player 3'},
            {id: 5, title: 'player 4'},
            {id: 6, title: 'player 5'},
            {id: 7, title: 'player 6'},
            {id: 8, title: 'player 7'}
        ];


        this.getPlayersList = function () {
            return players;
        }
    }]);
});
requirejs.config({
    "baseUrl": "/bingo",
    "paths": {
        "app": "js/app",
        'routes': 'js/routes',
        'lib': 'lib',
        'controllers': 'js/controllers',
        'services': 'js/services',
        'directives': 'js/directives',
        'service.configuration': ['js/services/configuration']
    },
    shim: {
        'lib/angular': {
            'exports': 'angular'
        }
    },
    priority: [
        "lib/angular"
    ]
});

var globalDependencies = [
    'lib/angular',
    'app',
    'routes',
    'service.configuration',
    'controllers/BingoCtrl',
    'controllers/FieldCtrl',
    'services/gameService',
    'services/roomService',
    'services/playerService',
    'services/timerService'
    // and all other controllers
];


require(globalDependencies, function (angular, app) {
    'use strict';
    angular.bootstrap(document, [app.name]);
});
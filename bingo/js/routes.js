define([ 'app' ], function (app) {
    'use strict';

        return app.config([ '$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
            $routeProvider.
                when('/', {templateUrl: 'partials/list.html', controller: 'BingoCtrl'}).
                when('/room/:id', {templateUrl: 'partials/room.html', controller: 'FieldCtrl'}).
                otherwise('/');
        } ]);
    });
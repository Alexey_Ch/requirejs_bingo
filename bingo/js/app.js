define([
    'lib/angular',
], function(angular) {
    'use strict';
    return angular.module('bingo', []);
});
define(['app'], function(app) {
    return app.controller('FieldCtrl', [ '$scope', '$http', 'configuration', 'gameService', 'playerService', function($scope, $http, configuration, gameService, playerService) {

        console.log("FieldCtrl.construct()");
        var game = gameService.createGame(1, $scope);
        $scope.game = game;
        $scope.players = playerService.getPlayersList();

        $scope.rows = game.field;
        $scope.header = game.winText;
        $scope.win = game.status;
        $scope.data = new Array();

        var cell;
        for(var j = 0; j < 5; j++) {
            for(var i = 0; i < 5; i++) {
                cell = $scope.rows[j][i];
               $scope.data.push(cell);
            }
        }
    }]);
});
define(['app'], function(app) {
    return app.controller('BingoCtrl', [ '$scope', '$http', 'configuration', 'roomService', 'timerService', function($scope, $http, configuration, roomService, timerService) {

        console.log("BingoCtrl.construct()");
        timerService.deleteTimers();
        $scope.rooms = roomService.getRoomList();
        $scope.message = "Hello, World!";
        $scope.message2 = "Hello, from bingo room!";

    }]);
});